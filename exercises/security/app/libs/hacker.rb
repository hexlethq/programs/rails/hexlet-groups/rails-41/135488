# frozen_string_literal: true

require 'open-uri'
require 'net/http'
require 'uri'

class Hacker
  SIGN_UP_FORM_URL = URI('https://rails-l4-collective-blog.herokuapp.com/users/sign_up').freeze
  SIGN_UP_URL = URI('https://rails-l4-collective-blog.herokuapp.com/users').freeze

  class << self
    def hack(email, password)
      # BEGIN
      sign_up_form = parse_form
      cookie = find_cookie(sign_up_form)
      token = find_token(sign_up_form)
      sign_up_request(token, cookie, email, password)
      # END
    end

    private

    def parse_form
      response = URI.open(SIGN_UP_FORM_URL)
      raise 'Form page missing' unless response.status.first == '200'

      response
    end

    def find_cookie(form)
      cookie = form.meta['set-cookie'].split('; ').first
      raise 'No cookie in form' unless cookie

      cookie
    end

    def find_token(form)
      html = Nokogiri::HTML(form)
      token = html.css('input[name="authenticity_token"]').first['value']
      raise 'No token in form' unless token

      token
    end

    def sign_up_request(token, cookie, email, password)
      params = {
        authenticity_token: token,
        user: { email: email, password: password, password_confirmation: password }
      }

      req = Net::HTTP::Post.new(SIGN_UP_URL)
      req.set_form_data(params)
      req['Cookie'] = cookie

      res = Net::HTTP.start(SIGN_UP_URL.hostname, SIGN_UP_URL.port, use_ssl: true) { |https| https.request(req) }

      case res
      when Net::HTTPSuccess, Net::HTTPRedirection
        res
      else
        raise 'Request failed'
      end
    end
  end
end
