# frozen_string_literal: true

require 'test_helper'

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  self.use_transactional_tests = true

  def setup
    @bulletin = bulletins(:first)
  end

  test 'GET #index' do
    get bulletins_url

    assert_response :success

    assert_select 'li:last-child', @bulletin.title
  end

  test 'GET #show success' do
    get bulletin_url(@bulletin)

    assert_response :success

    assert_select 'h3', @bulletin.title
    assert_select 'p', @bulletin.body
  end

  test 'GET #show failed' do
    @bulletin.id = bulletins.size + 1

    assert_raises(ActiveRecord::RecordNotFound) do
      get bulletin_url(@bulletin)
    end
  end
end
