# frozen_string_literal: true

class Web::ReviewsController < Web::Movies::ApplicationController
  def index
    @reviews = Review.limit(40).order(id: :desc)
  end
end
