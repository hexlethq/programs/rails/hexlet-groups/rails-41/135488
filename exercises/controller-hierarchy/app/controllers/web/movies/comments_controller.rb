# frozen_string_literal: true

class Web::Movies::CommentsController < Web::Movies::ApplicationController
  def index
    @comments = resource_movie.comments.limit(40).order(id: :desc)
  end

  def new
    @comment = resource_movie.comments.new
  end

  def create
    @comment = resource_movie.comments.build(comment_permitted_params)
    if @comment.save
      redirect_to resource_movie, notice: t('success')
    else
      render :new
    end
  end

  def edit
    @comment = resource_movie.comments.find(params[:id])
  end

  def update
    @comment = resource_movie.comments.find(params[:id])
    if @comment.update(comment_permitted_params)
      redirect_to resource_movie, notice: t('success')
    else
      render :edit, notice: t('fail')
    end
  end

  def destroy
    @comment = resource_movie.comments.find(params[:id])

    if @comment.destroy
      redirect_to resource_movie, notice: t('success')
    else
      redirect_to resource_movie, notice: t('fail')
    end
  end

  private

  def comment_permitted_params
    params.require(:comment).permit(:body)
  end
end
