# frozen_string_literal: true

class Web::Movies::ReviewsController < Web::Movies::ApplicationController
  def index
    @reviews = resource_movie.reviews.limit(40).order(id: :desc)
  end

  def show
    @review = resource_movie.reviews.find(params[:id])
  end

  def new
    @review = resource_movie.reviews.new
  end

  def create
    @review = resource_movie.reviews.build(review_permitted_params)
    if @review.save
      redirect_to [resource_movie, @review], notice: t('success')
    else
      render :new
    end
  end

  def edit
    @review = resource_movie.reviews.find(params[:id])
  end

  def update
    @review = resource_movie.reviews.find(params[:id])
    if @review.update(review_permitted_params)
      redirect_to [resource_movie, @review], notice: t('success')
    else
      render :edit, notice: t('fail')
    end
  end

  def destroy
    @review = resource_movie.reviews.find(params[:id])

    if @review.destroy
      redirect_to resource_movie, notice: t('success')
    else
      redirect_to resource_movie, notice: t('fail')
    end
  end

  private

  def review_permitted_params
    params.require(:review).permit(:body)
  end
end
