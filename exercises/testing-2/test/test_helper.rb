# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'

# BEGIN
require 'webmock/minitest'

WebMock.disable_net_connect!
# END

module ActiveSupport
  class TestCase
    # Run tests in parallel with specified workers
    parallelize(workers: :number_of_processors)

    # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
    fixtures :all

    # Add more helper methods to be used by all tests here...
    def load_fixture(filename)
      File.read(File.dirname(__FILE__) + "/fixtures/#{filename}")
    end

    def select_methods_starts_with(test_instance, substr)
      test_instance.methods.select { |method| method.start_with? substr }
    end

    def stub_git_request(url, status, body, headers)
      stub_request(:get, url)
        .with(
          headers: {
            'Accept' => 'application/vnd.github.v3+json',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Content-Type' => 'application/json',
            'User-Agent' => 'Octokit Ruby Gem 4.21.0'
          }
        ).to_return(status: status, body: body, headers: headers)
    end
  end
end
