# frozen_string_literal: true

require 'test_helper'

class Web::RepositoriesControllerTest < ActionDispatch::IntegrationTest
  # BEGIN
  setup do
    @link = 'https://github.com/login/repo_name'
    @updated_link = { link: 'https://github.com/login/another' }
    @stubed_link = 'https://api.github.com/repos/login/repo_name'
    @invalid_link = 'invalid_link'
    @attrs = JSON.parse(load_fixture('files/response.json'))

    stub_git_request(@stubed_link, 200, load_fixture('files/response.json'),
                     { content_type: 'application/json; charset=utf-8' })

    @repo = repositories :one
  end

  test 'get show' do
    get repository_url @repo

    assert_response :success
  end

  test 'get index' do
    get repositories_url

    assert_response :success
  end

  test 'get new' do
    get new_repository_url

    assert_response :success
  end

  test 'should create' do
    post repositories_url, params: { repository: { link: @link } }

    assert_response :redirect

    repo = Repository.find_by(link: @link)
    assert { repo }
    assert_equal @attrs['description'], repo.description
  end

  test 'error' do
    post repositories_url, params: { repository: { link: @invalid_link } }

    assert_redirected_to root_path

    repo = Repository.find_by(link: @invalid_link)
    assert { !repo }
  end

  test 'get edit' do
    get edit_repository_url @repo

    assert_response :success
  end

  test 'should update' do
    patch repository_url(@repo), params: { repository: @updated_link }

    assert_response :redirect

    @repo.reload

    assert { @repo.link == @updated_link[:link] }
  end

  test 'should destroy' do
    delete repository_url(@repo)

    assert_response :redirect

    refute { Repository.find_by(id: @repo.id) }
  end
  # END
end
