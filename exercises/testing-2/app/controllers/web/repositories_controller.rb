# frozen_string_literal: true

# BEGIN
class Web::GetRepoInfoService
  attr_reader :client

  def initialize(client = Octokit::Client.new)
    @client = client
  end

  def call(permitted_params)
    link = permitted_params[:link]
    return permitted_params if link.blank?

    repo = load_repo_resource(link)
    repo_data = repo.to_h

    {
      link: link,
      owner_name: repo_data[:owner][:login],
      repo_name: repo_data[:name],
      repo_created_at: repo_data[:created_at],
      repo_updated_at: repo_data[:updated_at],
      **repo_data.slice(:description, :default_branch, :watchers_count, :language)
    }
  end

  private

  def load_repo_resource(link)
    link_data = link.split('/').compact.last(2).join('/')
    client.repo(link_data)
  end
end
# END

class Web::RepositoriesController < Web::ApplicationController
  rescue_from Octokit::InvalidRepository, with: ->(exception) { redirect_to root_path, notice: exception }

  def index
    @repositories = Repository.all
  end

  def new
    @repository = Repository.new
  end

  def show
    @repository = Repository.find params[:id]
  end

  def create
    # BEGIN
    repo_data = Web::GetRepoInfoService.new.call(permitted_params)
    @repository = Repository.new(repo_data)
    if @repository.save
      redirect_to repositories_path, notice: t('success')
    else
      render :new, notice: t('fail')
    end
    # END
  end

  def edit
    @repository = Repository.find params[:id]
  end

  def update
    @repository = Repository.find params[:id]

    if @repository.update(permitted_params)
      redirect_to repositories_path, notice: t('success')
    else
      render :edit, notice: t('fail')
    end
  end

  def destroy
    @repository = Repository.find params[:id]

    if @repository.destroy
      redirect_to repositories_path, notice: t('success')
    else
      redirect_to repositories_path, notice: t('fail')
    end
  end

  private

  def permitted_params
    params.require(:repository).permit(:link)
  end
end
