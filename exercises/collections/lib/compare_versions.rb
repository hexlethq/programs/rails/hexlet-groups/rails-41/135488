# frozen_string_literal: true

# BEGIN
def compare_versions(ver1, ver2)
  ver1_array, ver2_array =
    [ver1, ver2].map { |ver| ver.split('.').map(&:to_i) }

  result = nil

  ver1_array.each_with_index do |item, index|
    result = item <=> ver2_array[index] if index.zero? || result.zero?
  end

  result
end
# END
