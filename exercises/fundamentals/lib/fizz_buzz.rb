# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop, acc = '')
  return acc if start > stop

  left = (start % 3).zero? ? 'Fizz' : ''
  right = (start % 5).zero? ? 'Buzz' : ''
  result = "#{left}#{right}"

  output = result == '' ? start : result
  separator = acc == '' ? '' : ' '
  new_acc = "#{acc}#{separator}#{output}"

  fizz_buzz(start + 1, stop, new_acc)
end
# END
