# frozen_string_literal: true

# BEGIN
def reverse(string)
  string == '' ? '' : "#{reverse(string[1..])}#{string[0]}"
end
# END
