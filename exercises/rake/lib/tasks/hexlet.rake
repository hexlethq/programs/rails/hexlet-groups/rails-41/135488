# frozen_string_literal: true

require 'csv'

namespace :hexlet do
  desc 'Load users form csv-file'

  task :import_users, [:filepath] => :environment do |_t, args|
    abs_path = Rails.root.join(args[:filepath])
    print "Task started, path to csv-file: #{abs_path}\n"

    users = []
    File.open(abs_path) do |content|
      CSV.parse(content, headers: true) { |row| users << row.to_hash }
    end

    print "#{users.size} users parsed from file\n"

    User.create!(users)

    print "Task ended, #{User.count} users in database\n"
  end
end
