# frozen_string_literal: true

3.times do |i|
  Article.create(
    { title: "title_#{i}", body: "body_#{i}" }
  )
end
