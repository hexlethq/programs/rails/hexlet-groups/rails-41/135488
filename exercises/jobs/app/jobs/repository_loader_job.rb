# frozen_string_literal: true

class RepositoryLoaderJob < ApplicationJob
  queue_as :default

  def perform(repo_id)
    RepositoryLoaderService.new.call(repo_id)
  end
end
