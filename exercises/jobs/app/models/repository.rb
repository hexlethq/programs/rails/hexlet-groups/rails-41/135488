# frozen_string_literal: true

class Repository < ApplicationRecord
  include AASM

  validates :link, presence: true, uniqueness: true

  # BEGIN
  aasm do
    state :created, initial: true
    state :fetching
    state :fetched
    state :failed

    event :start_loading do
      transitions from: [:created, :fetched, :failed], to: :fetching
    end

    event :finish_fetched do
      transitions from: :fetching, to: :fetched
    end

    event :finish_failed do
      transitions from: :fetching, to: :failed
    end
  end
  # END
end
