# frozen_string_literal: true

class RepositoryLoaderService
  attr_reader :client

  def initialize(client = Octokit::Client.new)
    @client = client
  end

  # repo insted id?
  def call(id)
    repo = Repository.find(id)
    repo.start_loading!
    repo_data = load_repo_resource!(repo.link).to_h
  rescue Octokit::InvalidRepository, Octokit::NotFound => error
    repo.finish_failed!
    Rails.logger.error error.message
  rescue StandardError => error
    repo.finish_failed!
    Rails.logger.error "Unexpected error: #{error.message}"
  else
    updated_params = {
      owner_name: repo_data[:owner][:login],
      repo_name: repo_data[:name],
      repo_created_at: repo_data[:created_at],
      repo_updated_at: repo_data[:updated_at],
      **repo_data.slice(:description, :default_branch, :watchers_count, :language)
    }

    repo.update!(updated_params.merge(aasm_state: :fetched))
  end

  private

  def load_repo_resource!(link)
    link_data = link.split('/').compact.last(2).join('/')
    client.repo(link_data)
  end
end
