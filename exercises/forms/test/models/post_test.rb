# frozen_string_literal: true

require 'test_helper'

class PostTest < ActiveSupport::TestCase
  self.use_transactional_tests = true

  def setup
    @post = posts(:one)
    @presence_message = "can't be blank"
  end

  test 'valid' do
    assert @post.valid?
  end

  test 'invalid without title' do
    @post.title = nil

    refute @post.valid?
    assert_equal [@presence_message], @post.errors[:title]
  end

  test 'invalid without body' do
    @post.body = nil

    refute @post.valid?
    assert_equal [@presence_message], @post.errors[:body]
  end
end
