# frozen_string_literal: true

class PostsController < ApplicationController
  before_action :load_post, only: %i[show edit update destroy]

  def index
    # @posts = Post.where(published: true).order(:created_at)
    @posts = Post.all.order(:created_at)
  end

  def show; end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      redirect_to @post
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit; end

  def update
    if @post.update(post_params)
      redirect_to @post
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @post.destroy

    redirect_to posts_path
  end

  private

  def post_params
    params.require(:post).permit(:title, :body, :summary, :published)
  end

  def load_post
    @post = Post.find(params[:id])
  end
end
