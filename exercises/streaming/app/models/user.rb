# frozen_string_literal: true

class User < ApplicationRecord
  CSV_FILE_NAME = 'report.csv'

  validates :name, presence: true
  validates :email, presence: true
end
