# frozen_string_literal: true

require 'test_helper'

module HexletCheck
  class PostsControllerTest < ActionDispatch::IntegrationTest
    setup do
      @post = posts(:without_comments)
    end

    test 'should get index' do
      get posts_url(locale: :en)
      assert_response :success
    end

    test 'should get new' do
      get new_post_url(locale: :en)
      assert_response :success
    end

    test 'should create post' do
      assert_difference('Post.count') do
        post posts_url(locale: :en), params: { post: {
          title: 'Title',
          body: 'Body'
        } }
      end

      assert_redirected_to post_url(Post.last, locale: :en)
    end

    test 'should show post' do
      get post_url(@post, locale: :en)
      assert_response :success
    end

    test 'should get edit' do
      get edit_post_url(@post, locale: :en)
      assert_response :success
    end

    test 'should update post' do
      patch post_url(@post, locale: :en), params: { post: {
        title: 'Title',
        body: 'Body'
      } }
      assert_redirected_to post_url(@post, locale: :en)
    end

    test 'should destroy post' do
      assert_difference('Post.count', -1) do
        delete post_url(@post, locale: :en)
      end

      assert_redirected_to posts_url(locale: :en)
    end
  end
end
