# frozen_string_literal: true

Rails.application.routes.draw do
  # BEGIN
  root 'home#index'

  resources :posts do
    scope module: :posts do
      resources :comments
    end
  end
  # END
end
