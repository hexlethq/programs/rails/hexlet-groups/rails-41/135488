# frozen_string_literal: true

class AdminPolicy
  STATUS_FORBIDDEN = 403

  def initialize(app)
    @app = app
  end

  def call(env)
    request = Rack::Request.new(env)
    handle(request)
  end

  private

  def handle(request)
    request.path.start_with?('/admin') ? handle_forbidden : handle_next(request)
  end

  def handle_forbidden
    [STATUS_FORBIDDEN, {}, []]
  end

  def handle_next(request)
    @app.call(request.env)
  end
end
