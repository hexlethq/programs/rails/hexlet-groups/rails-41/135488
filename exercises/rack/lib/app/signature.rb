# frozen_string_literal: true

require 'digest'

class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, body = @app.call(env)
    updated_body = hash_of(body)

    [status, headers, updated_body]
  end

  private

  def hash_of(body)
    body_hash = Digest::SHA256.hexdigest(body)
    [body, body_hash].join("\n")
  end
end
