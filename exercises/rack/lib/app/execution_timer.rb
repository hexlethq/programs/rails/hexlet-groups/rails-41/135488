# frozen_string_literal: true

require 'benchmark'

class ExecutionTimer
  def initialize(app)
    @app = app
  end

  def call(env)
    response = nil
    response_time = Benchmark.realtime { response = @app.call(env) }

    make_response(response, response_time)
  end

  private

  def make_response(response, response_time)
    status, headers, body = response
    updated_body = body_with_response_time(body, response_time)

    [status, headers, updated_body]
  end

  def body_with_response_time(body, time)
    time_in_microseconds = (time * 1_000_000).round
    time_info = "Response time: #{time_in_microseconds}"

    [body, time_info].join("\n")
  end
end
