# frozen_string_literal: true

class Router
  STATUS_OK = 200
  STATUS_NOT_FOUND = 404

  BODY_ROOT = 'Hello, World!'
  BODY_ABOUT = 'About page'
  BODY_NOT_FOUND = '404 Not Found'

  def call(env)
    request = Rack::Request.new(env)
    handle(request)
  end

  private

  def handle(request)
    case request.path
    when '/about' then handle_about
    when '/' then handle_root
    else handle_not_found
    end
  end

  def handle_about
    make_response(STATUS_OK, BODY_ABOUT)
  end

  def handle_root
    make_response(STATUS_OK, BODY_ROOT)
  end

  def handle_not_found
    make_response(STATUS_NOT_FOUND, BODY_NOT_FOUND)
  end

  def make_response(status, body)
    [status, headers, body]
  end

  def headers
    { 'Content-Type' => 'text/plain' }
  end
end
