# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stock = Stack.new
  end

  def test_basic_functions
    @stock.push!('a')
    @stock.push!('b')

    assert_equal @stock.pop!, 'b'
    assert_equal @stock.pop!, 'a'
  end

  def test_empty
    assert @stock.empty?

    @stock.push!('a')

    refute @stock.empty?
  end

  def test_clear
    @stock.push!('a')
    @stock.clear!

    assert @stock.empty?
  end

  def test_to_a_and_size
    assert_equal @stock.to_a, []
    assert_equal @stock.size, 0

    @stock.push!('a')
    @stock.push!('b')

    assert_equal @stock.to_a, %w[a b]
    assert_equal @stock.size, 2
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
