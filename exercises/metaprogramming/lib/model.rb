# frozen_string_literal: true

# BEGIN
require 'date'

module Model
  def self.included(base)
    base.extend ClassMethods
    base.include InstanceMethods
  end

  module ClassMethods
    attr_reader :schema, :mapping

    MAPPING = {
      string: ->(value) { value.nil? ? nil : value.to_s },
      integer: ->(value) { value.nil? ? nil : value.to_i },
      boolean: ->(value) { value.nil? ? nil : !value == false },
      datetime: ->(value) { value.nil? ? nil : DateTime.parse(value) }
    }.freeze

    def attribute(name, options = {}) # rubocop:disable Metrics/AbcSize
      @schema ||= {}
      @schema[name] = options

      define_method(name) do
        attr = "@#{name}".to_sym
        value = instance_variable_get(attr)

        value.nil? && !instance_variables.include?(attr) ? self.class.schema[name][:default] : value
      end

      define_method("#{name}=") do |value|
        type = self.class.schema[name][:type]
        new_value = type.nil? ? value : MAPPING[type].call(value)

        instance_variable_set("@#{name}", new_value)
      end
    end
  end

  module InstanceMethods
    def initialize(attrs = {})
      attrs
        .select { |attr| self.class.schema.key?(attr) }
        .each { |key, value| public_send("#{key}=", value) }
    end

    def attributes
      self.class.schema.keys.each_with_object({}) { |key, acc| acc[key] = public_send(key) }
    end
  end
end
# END
