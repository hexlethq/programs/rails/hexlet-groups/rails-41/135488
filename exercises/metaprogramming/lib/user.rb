# frozen_string_literal: true

require_relative './model'
require 'date'

class User
  include Model

  attribute :name, default: 'Andrey'
  attribute :birthday, type: :datetime
  attribute :active, type: :boolean, default: false
end
