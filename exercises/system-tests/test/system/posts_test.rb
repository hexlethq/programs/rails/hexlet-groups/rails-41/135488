# frozen_string_literal: true

require 'application_system_test_case'

# BEGIN
class PostsTest < ApplicationSystemTestCase
  setup do
    @post = posts(:one)
    @another_post = posts(:without_comments)

    visit posts_url
  end

  test 'see all posts' do
    assert_selector 'h1', text: 'Posts'
    Post.all { |post| assert_text post.title }
  end

  test 'create a post' do
    click_on 'New Post'

    fill_in 'Title', with: @post.title
    fill_in 'Body', with: @post.body
    click_on 'Create'

    assert_text 'Post was successfully created'
    assert_selector 'p', text: @post.body
  end

  test 'create a post without title' do
    click_on 'New Post'
    fill_in 'Body', with: @post.body
    click_on 'Create'

    assert_text "Title can't be blank"
  end

  test 'show the post' do
    find('tr', text: @post.title).click_link('Show')

    assert_selector 'p', text: @post.body
  end

  test 'update the post' do
    find('tr', text: @post.title).click_link('Edit')
    fill_in 'Body', with: @another_post.body
    click_on 'Update Post'

    assert_text 'Post was successfully updated'
    assert_selector 'p', text: @another_post.body
  end

  test 'update the post without title' do
    find('tr', text: @post.title).click_link('Edit')
    fill_in 'Title', with: ''
    click_on 'Update Post'

    assert_text "Title can't be blank"
  end

  test 'destroy the post' do
    accept_confirm do
      find('tr', text: @another_post.title).click_link('Destroy')
    end

    assert_text 'Post was successfully destroyed'
    assert_no_text @another_post.title
  end
end
# END
