# frozen_string_literal: true

Rails.application.routes.draw do
  # BEGIN
  root 'tasks#index'

  resources :tasks
  # END
end
