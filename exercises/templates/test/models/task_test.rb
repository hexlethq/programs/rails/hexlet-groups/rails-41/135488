# frozen_string_literal: true

require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  self.use_transactional_tests = true

  def setup
    @task = tasks(:one)
    @presence_message = "can't be blank"
  end

  test 'valid' do
    assert @task.valid?
  end

  test 'invalid without name' do
    @task.name = nil

    refute @task.valid?
    assert_equal [@presence_message], @task.errors[:name]
  end

  test 'invalid without status' do
    @task.status = nil

    refute @task.valid?
    assert_equal [@presence_message], @task.errors[:status]
  end

  test 'invalid without creator' do
    @task.creator = nil

    refute @task.valid?
    assert_equal [@presence_message], @task.errors[:creator]
  end
end
