# frozen_string_literal: true

require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  self.use_transactional_tests = true

  def setup
    @task1 = tasks(:one)
    @task2 = tasks(:two)
    @valid_attrs = { name: 'Bill', description: 'valid task', creator: 'Fane', completed: true }
    @invalid_attrs = { creator: 'Fane', completed: true }
  end

  test 'GET #index' do
    get tasks_url

    assert_response :success

    assert_select 'section ul li', 2
    assert_select 'section ul li:first-child a', @task1.name
  end

  test 'GET #show success' do
    get task_url(@task1)

    assert_response :success
    assert_select 'h3', @task1.name
  end

  test 'GET #show failed' do
    assert_raises(ActiveRecord::RecordNotFound) do
      get task_url(tasks.size + 1)
    end
  end

  test 'GET #new' do
    get new_task_url

    assert_response :success
    assert_select 'h2', 'New task'
  end

  test 'POST #create success' do
    post tasks_url, params: { task: @valid_attrs }

    task = Task.last

    assert_redirected_to task_path(task)
    assert_equal 3, Task.count
    assert_equal @valid_attrs[:name], task.name
  end

  test 'POST #create failed' do
    post tasks_url, params: { task: @invalid_attrs }

    assert_response :unprocessable_entity
    assert_equal 2, Task.count
  end

  test 'GET #edit' do
    get edit_task_url(@task1)

    assert_response :success
    assert_select 'h2', 'Edit task'
  end

  test 'PATCH #update success' do
    patch task_url(@task1), params: { task: @valid_attrs }

    assert_redirected_to task_path(@task1)
    assert_equal @valid_attrs[:name], Task.first.name
  end

  test 'PATCH #update failed' do
    post tasks_url, params: { task: @invalid_attrs }

    assert_response :unprocessable_entity
    assert_equal @task1[:name], Task.first.name
  end

  test 'DELETE #destroy' do
    delete task_url(@task1)

    assert_redirected_to tasks_path
    assert_equal 1, Task.count
    assert_equal @task2.name, Task.first.name
  end
end
