# frozen_string_literal: true

class Vehicle < ApplicationRecord
  # BEGIN
  has_one_attached :image
  # END

  validates :manufacture, presence: true
  validates :model, presence: true
  validates :color, presence: true
  validates :doors, presence: true, numericality: { only_integer: true }
  validates :kilometrage, presence: true, numericality: { only_integer: true }
  validates :production_year, presence: true

  # BEGIN
  validates :image, size: { less_than: 5.megabytes, message: 'should be less than 5 megabytes' },
                    content_type: { in: %i[png jpg jpeg], message: 'should be jpeg or jpg or png' },
                    attached: true
  # END
end
