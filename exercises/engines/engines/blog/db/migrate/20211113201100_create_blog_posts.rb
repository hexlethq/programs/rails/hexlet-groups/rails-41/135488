# frozen_string_literal: true

class CreateBlogPosts < ActiveRecord::Migration[6.1]
  def change
    create_table :blog_posts do |t|
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
