# frozen_string_literal: true
                                                                                           #
Gem::Specification.new do |spec|
  spec.name        = 'blog'
  spec.version     = '0.1.0'
  spec.authors     = ['yorickov']
  spec.email       = ['yorickov@gmail.com']
  spec.homepage    = 'https://hexlet.io'
  spec.summary     = 'Blog'
  spec.description = 'Nice Blog.'
  spec.license     = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata['allowed_push_host'] = 'https://github.com/hexlet'

  spec.metadata['homepage_uri'] = spec.homepage
  # spec.metadata["source_code_uri"] = "TODO: Put your gem's public repo URL here."
  # spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  spec.files = Dir["{app,config,db,lib}/**/*", 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_dependency 'rails', '~> 6.1.4', '>= 6.1.4.1'
  spec.add_dependency 'slim-rails'
  spec.add_dependency 'puma'
end
