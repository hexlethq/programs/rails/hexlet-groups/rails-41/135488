# frozen_string_literal: true
# This migration comes from blog (originally 20211113201100)

class CreateBlogPosts < ActiveRecord::Migration[6.1]
  def change
    create_table :blog_posts do |t|
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
