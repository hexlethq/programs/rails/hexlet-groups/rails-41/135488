# frozen_string_literal: true

class SetLocaleMiddleware
  # BEGIN
  def initialize(app)
    @app = app
  end

  def call(env)
    @request = Rack::Request.new(env)

    locale = extract_locale_from_accept_header
    switch_locale(locale)

    app.call(env)
  end

  private

  attr_reader :app, :request

  def switch_locale(locale)
    start_log = if locale.nil?
                  "No locale in header, default locale will be set: '#{I18n.default_locale}'"
                else
                  "Locale from header: '#{locale}', it will replace default locale: '#{I18n.default_locale}'"
                end

    Rails.logger.debug "** #{start_log} **"
    I18n.locale = locale || I18n.default_locale
    Rails.logger.debug "** Locale successfully set to '#{I18n.locale}'**"
  end

  def extract_locale_from_accept_header
    request.fetch_header('HTTP_ACCEPT_LANGUAGE') { '' }.scan(/^[a-z]{2}/).first
  end
  # END
end
