# frozen_string_literal: true

Rails.application.routes.draw do
  resources :users
  resources :tasks

  root 'welcome#index'
end
