# frozen_string_literal: true

module Posts
  class CommentsController < ApplicationController
    before_action :load_comment, only: %i[edit update destroy]
    before_action :load_post

    def create
      @comment = @post.comments.build(comment_params)
      if @comment.save
        redirect_to @post, notice: 'Comment created'
      else
        flash.now[:notice] = "Comment can't be blank"
        render 'posts/show', status: :unprocessable_entity
      end
    end

    def edit; end

    def update
      if @comment.update(comment_params)
        redirect_to @comment.post, notice: 'Comment updated'
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @comment.destroy
      redirect_to @post, notice: 'Comment destroyed'
    end

    private

    def comment_params
      params.require(:post_comment).permit(:body)
    end

    def load_comment
      @comment = Post::Comment.find(params[:id])
    end

    def load_post
      @post = Post.find(params[:post_id])
    end
  end
end
