# frozen_string_literal: true

# BEGIN
def sort_word(word)
  word.chars.sort.join
end

def anagramm_filter(check_word, words)
  check_word_sorted = sort_word(check_word)
  words.select { |word| check_word_sorted == sort_word(word) }
end
# END
