# frozen_string_literal: true

# BEGIN
def get_same_parity(list)
  return [] if list.empty?

  is_first_even = list.first.even?
  list.select { |num| num.even? == is_first_even }
end
# END
