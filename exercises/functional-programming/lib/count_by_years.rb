# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  users
    .select { |item| item[:gender] == 'male' }
    .group_by { |item| item[:birthday].split('-').first }
    .transform_values(&:size)
end
# END
